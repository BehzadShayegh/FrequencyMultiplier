module controller (input refclk, rst,
	input adjust, sip,
	output reg valid, cnt_enM, cnt_initM, init_out, shift);

	reg [2:0] present_stage, next_stage;

	always @(adjust,sip) begin
		valid = 0;
		cnt_enM = 0;
		cnt_initM = 0;
		init_out = 0;
		shift = 0;
		next_stage = 3'b 0;

		case(present_stage)
			0: begin
				valid = 1;
				if(adjust==0) next_stage = present_stage;
				else if(adjust==1) next_stage = 1;
				else next_stage = present_stage;
			end
			1: begin
				init_out = 1;
				if(adjust==0) next_stage = 2;
				else if(adjust==1) next_stage = present_stage;
				else next_stage = present_stage;
			end
			2: begin
				cnt_initM = 1;
				if(sip==0) next_stage = present_stage;
				else if(sip==1) next_stage = 3;
				else next_stage = present_stage;
			end
			3: begin
				cnt_enM = 1;
				if(sip==0) next_stage = present_stage;
				else if(sip==1) next_stage = 4;
				else next_stage = present_stage;
			end
			4: begin
				shift = 1;
				next_stage = 0;
			end
			default: next_stage = 0;
		endcase

	end

	always @(posedge refclk, posedge rst) begin
		if(rst) present_stage = 0;
		else present_stage = next_stage;
	end

endmodule

