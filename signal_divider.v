module signal_divider(input refclk, rst, init_out, input [15:0] k, inout reg [15:0] trash, output reg outSignal);

	always @(posedge refclk, posedge rst) begin
		if(rst) trash = 0;
		else begin
			outSignal = 0;
			if(init_out) trash = k;
			else if(trash == 16'b 0) begin
				outSignal = 1;
				trash = k;	
			end
			else trash = trash - 1;
		end
	end
	
endmodule