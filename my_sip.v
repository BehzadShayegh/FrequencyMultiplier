module my_sip(input inSignal, dff_out, output sip);

	assign sip = (inSignal & ~dff_out);
	
endmodule