module my_dff(input refclk, rst, inSignal, output reg dff_out);

	always @(posedge refclk, posedge rst) begin
			if(rst) dff_out = 0;
			else dff_out = inSignal;
		end
		
endmodule