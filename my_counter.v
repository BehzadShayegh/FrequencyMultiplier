module my_counter(input refclk, rst, cnt_initM, cnt_enM, inout reg [15:0] m);

	always @(posedge refclk, posedge rst) begin
		if(rst) m = 16'b 0;
		else begin
			if(cnt_initM) m = 16'b 0;
			else if(cnt_enM) m = m+1;
			else m = m;
		end
	end
	
endmodule