module myshifter(input refclk, rst, shift, input [4:0] n, input [15:0] m, inout reg [15:0] k);

	always @(posedge refclk, posedge rst) begin
		if(rst) k = 16'b 0;
		else if(shift) k = m>>n;
		else k = k;
	end
	
endmodule